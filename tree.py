#!/usr/bin/env python3.6

import cmd
from docopt import docopt, DocoptExit
import os
import struct
# Working directory
cwd = os.path.dirname(os.path.abspath(__file__))


"""
This example uses docopt with the built in cmd module to demonstrate an
interactive command application.

Usage:
    tree (-i | --interactive)
    tree (-h | --help | --version)
    tree folder_structure  [--directory=<dir>]

Options:
    -h, --help                Show this screen and exit.
    -i, --interactive         Interactive Mode
    -d, --directory=<dir>     Odredjuje folder u kome ce se stvoriti fajl
"""


def docopt_cmd(func):
    """
    This decorator is used to simplify the try/except block and pass the result
    of the docopt parsing to the called action.
    """

    def fn(self, arg):
        """ Wrapper for reading passed arguments and handling exceptions """
        try:
            options = docopt(fn.__doc__, arg)
            if '--directory' in options:
                d = options['--directory']
                options['--directory'] = cwd if d is None else d

        except DocoptExit as e:
            # The DocoptExit is thrown when the args do not match.
            # We print a message to the user and the usage block.
            print(e)
            return

        except SystemExit:
            # The SystemExit exception prints the usage for --help
            # We do not need to do the print here.
            return

        try:
            # Ovde uhvati sve moguce greske koje mozes da bi funkcije ostale
            # sto je moguce vise ciste od nepotrebnog koda
            return func(self, options)

        except FileNotFoundError as e:
            print(e)
            return

    fn.__name__ = func.__name__
    fn.__doc__ = func.__doc__
    fn.__dict__.update(func.__dict__)
    return fn


def get_terminal_size():
    """ Gets terminal size """
    from ctypes import windll, create_string_buffer
    # stdin handle is -10
    # stdout handle is -11
    # stderr handle is -12
    h = windll.kernel32.GetStdHandle(-12)
    csbi = create_string_buffer(22)
    res = windll.kernel32.GetConsoleScreenBufferInfo(h, csbi)
    if res:
        (bufx, bufy, curx, cury, wattr,
         left, top, right, bottom,
         maxx, maxy) = struct.unpack("hhhhHhhhhhh", csbi.raw)
        sizex = right - left + 1
        sizey = bottom - top + 1
        return sizex, sizey


class Tree(cmd.Cmd):
    """ Set pomocnih alata za rad sa WCC bazom i IMOS  softverom """

    width = get_terminal_size()[0]
    intro = ('\n' + ' Tree 1.0 '.center(width, '=') +
             ''.center(width, '-') +
             'write "help" for a list of available commands'.center(width, ' '))

    prompt = '►►► '
    file = None

# Methods/Commands
# region

    @docopt_cmd
    def do_folder_structure(self, arg):
        """Usage: folder_structure [--directory=<dir>]

           Options:
           -d, --directory=<dir>     Odredjuje folder u kome ce se stvoriti fajl
        """

        print(arg)


Tree().cmdloop()
